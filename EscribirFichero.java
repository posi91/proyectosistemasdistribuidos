import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class EscribirFichero {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner(System.in);
		System.out.println("Digite numero de cuenta");
		String cuenta = lector.nextLine();
		System.out.println("Valor a Retirar");
		String valor_retiro = lector.nextLine();
		lector.close();
		//Convertimos las
		int vr = Integer.parseInt(valor_retiro);
		int c = Integer.parseInt(cuenta);
		System.out.println("Registro grabado OK \n");
		System.out.println("Su numero de cuenta es : " +  cuenta);
		System.out.println("Su deposito es de : " +  valor_retiro);


		try
		{
		//Crear un objeto File se encarga de crear o abrir acceso a un archivo que se especifica en su constructor
		File archivo=new File("/tmp/datos.txt");

		//Crear objeto FileWriter que sera el que nos ayude a escribir sobre archivo
		FileWriter escribir=new FileWriter(archivo,true);

		//Escribimos en el archivo con el metodo write
		escribir.write(vr + "," + c + "\n");

		//Cerramos la conexion
		escribir.close();
		}

		//Si existe un problema al escribir cae aqui
		catch(Exception e)
		{
		System.out.println("NO-OK");
		}

	}

}
